from flask import Flask, render_template, request, redirect
import psycopg
from psycopg.rows import dict_row


app = Flask(__name__)


def fetch_vouchers():
    # Connect to an existing database
    with psycopg.connect(
        "dbname=vouchers user=postgres password=password host=127.0.0.1"
    ) as conn:
        # Open a cursor to perform database operations
        with conn.cursor(row_factory=dict_row) as cur:
            # Query the database and obtain data as Python objects.
            cur.execute("SELECT * FROM vouchers")
            return cur.fetchall()


def write_voucher(d):
    with psycopg.connect(
        "dbname=vouchers user=postgres password=password host=127.0.0.1"
    ) as conn:
        with conn.cursor() as cur:
            # Pass data to fill a query placeholders and let Psycopg perform
            # the correct conversion (no SQL injections!)
            cur.execute(
                "INSERT INTO vouchers (debit, credit, user_id) VALUES (%s, %s, %s)",
                (d["debit"], d["kredit"], d["user_id"]),
            )
            conn.commit()


@app.route("/")
def show_index():
    rows = fetch_vouchers()
    print(rows)
    return render_template("index.html", vouchers=rows)


@app.route("/voucher", methods=["POST"])
def add_voucher():
    print(request.form)
    write_voucher(request.form)
    return redirect("/")


if __name__ == "__main__":
    app.run(port=8000, debug=True)
