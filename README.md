# minimal flask example



## Getting started
Assumes a postgres db server on `127.0.0.1:5432` with user `postgres` and password `password`. Get one for free: `docker run -it --rm -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -p 5432:5432 postgres:15`
Server should have a database `vouchers`. Run the `migrations.sql` file on that database.

In a terminal:
1. `virtualenv venv`
1. `source venv/bin/activate`
1. `pip install -r requirements.txt`
1. `python main.py`
1. Visit http://127.0.0.1:8000

The form is terrible, so you need to enter values in all three fields.
