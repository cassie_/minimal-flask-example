CREATE TABLE vouchers (
  id int GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
  debit int NOT NULL,
  credit int NOT NULL,
  user_id int NOT NULL,
  created_at timestamptz NOT NULL DEFAULT now()
);